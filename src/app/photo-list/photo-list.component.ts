import { Component, OnInit } from '@angular/core';
import { PhotosService } from '../photos.service';
import { Observable } from 'rxjs';
import { Photo } from '../photo';
@Component({
  selector: 'app-photo-list',
  templateUrl: './photo-list.component.html',
  styleUrls: ['./photo-list.component.scss']
})
export class PhotoListComponent implements OnInit {

  photos: Observable<Photo[]>;
  constructor(private readonly photosService: PhotosService) { }

  ngOnInit() {
    this.photos = this.photosService.getPictures();
  }

}
