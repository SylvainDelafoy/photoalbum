import { Component, OnInit, Input } from '@angular/core';
import { MatDialog } from '@angular/material';
import { Photo } from '../photo';
import { PhotoDialogComponent } from '../photo-dialog/photo-dialog.component';

@Component({
  selector: 'app-photo',
  templateUrl: './photo.component.html',
  styleUrls: ['./photo.component.scss']
})
export class PhotoComponent {

  @Input() value: Photo;

  constructor(public dialog: MatDialog) {}

  openDialog() {
    const dialogRef = this.dialog.open(PhotoDialogComponent, {
       data: this.value ,
       panelClass: 'photoframe',
       maxWidth: '',
      });
  }

}
