import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialog } from '@angular/material';
import { Photo } from '../photo';
import { state, style, trigger } from '@angular/animations';

@Component({
  selector: 'app-photo-dialog',
  templateUrl: './photo-dialog.component.html',
  styleUrls: ['./photo-dialog.component.scss'],
})
export class PhotoDialogComponent implements OnInit {
  isOpen = true;

  constructor(
    @Inject(MAT_DIALOG_DATA) public value: Photo,
    dialog: MatDialog
    ) { }

  ngOnInit() {
  }
  imgClick() {
    this.isOpen = !this.isOpen;
  }
}
