import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, combineLatest, BehaviorSubject } from 'rxjs';
import { map, publishBehavior, publishReplay, refCount } from 'rxjs/operators';
import { Photo, Data } from './photo';

@Injectable({
  providedIn: 'root'
})
export class PhotosService {
  private $filter = new BehaviorSubject('');
  private readonly data: Observable<Data>;
  constructor(private http: HttpClient) {
    this.data = this.http.get<Data>('assets/photos.json')
    .pipe(
      publishReplay(1),
      refCount());
  }


  newFilter(text: string) {
    this.$filter.next(text);
  }

  getTitle(): Observable<string> {
    return this.data
    .pipe(map(x => x.title));
  }
  getPictures(): Observable<Photo[]> {
    return combineLatest(this.data, this.$filter).pipe(
      map(([arr, fil]) =>
        arr.photos.filter(x => [...x.tags, x.desc, x.title].join(' ').includes(fil))
      )
    );
  }
}
