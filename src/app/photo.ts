export interface Photo {
  readonly src: string;
  readonly title: string;
  readonly desc: string;
  readonly tags: string[];
}

export interface Data {
  title: string;
  photos: Photo[];
}
