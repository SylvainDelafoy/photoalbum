import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { TestBed } from '@angular/core/testing';

import { PhotosService } from './photos.service';
import { HttpClient } from '@angular/common/http';
import { Photo } from './photo';

describe('PhotosService', () => {
  let httpClient: HttpClient;
  let httpTestingController: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [ HttpClientTestingModule ]
    });

    // Inject the http service and test controller for each test
    httpClient = TestBed.get(HttpClient);
    httpTestingController = TestBed.get(HttpTestingController);
  });
  it('should be created', () => {
    const service: PhotosService = TestBed.get(PhotosService);
    expect(service).toBeTruthy();
  });
  it('should have expected URL', () => {
    const testData: Photo[] = [new Photo('https://via.placeholder.com/350x65', 'title', 'desc', ['tag1'])];

    const service: PhotosService = new PhotosService(httpClient);
    service.getPictures().pipe(

    ).subscribe(n => expect(n[0].src).toBe('https://via.placeholder.com/350x65'));

    const req = httpTestingController.expectOne('photos.json');
    expect(req.request.method).toEqual('GET');


    req.flush(testData);
    httpTestingController.verify();
  });
});
