import { Component } from '@angular/core';
import { PhotosService } from './photos.service';
import { Title } from '@angular/platform-browser';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title: Observable<string>;

  constructor(private photoService: PhotosService) {
    this.title = photoService.getTitle();
  }

  pushfilter( value: string) {
    this.photoService.newFilter(value);
  }
}
